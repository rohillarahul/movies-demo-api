const { Op, Sequelize } = require('sequelize');
const Movie = require('../models/Movie');
const Director = require('../models/Director');

function getAllMovies(req, res) {
  Movie.findAll({
    raw: true,
    include: [{
      model: Director,
      attributes: [
        'name',
      ],
      where: {
        state: Sequelize.col('director.id', Op.eq, 'movie.director_id'),
      },
    }],
  })
    .then((allDirector) => {
      res.status(200).json(allDirector);
    }).catch((error) => {
      res.status(500).json(error);
    });
}

function getMovieById(req, res) {
  const { movieId } = req.params;
  Movie.findOne({
    where: {
      id: movieId,
    },
    raw: true,
    include: [{
      model: Director,
      attributes: [
        'name',
      ],
      where: {
        state: Sequelize.col('director.id', Op.eq, 'movie.director_id'),
      },
    }],
  })
    .then((allDirector) => {
      if (allDirector) res.status(200).json(allDirector);
      else res.status(400).json({ msg: `${movieId} id is not present ` });
    }).catch((error) => {
      res.status(500).json(error);
    });
}

function addNewMovie(req, res) {
  const movie = req.body;
  const movieObject = {
    rank: movie.Rank,
    title: movie.Title,
    description: movie.Description,
    runtime: movie.Runtime,
    genre: movie.Genre,
    rating: movie.Rating,
    metascore: movie.Metascore === 'NA' ? 0 : movie.Metascore,
    votes: movie.Votes,
    gross_earning: movie.Gross_Earning_in_Mil === 'NA' ? 0 : movie.Gross_Earning_in_Mil,
    actor: movie.Actor,
    year: movie.Year,
  };
  const director = movie.Director;
  return Director.findOne({
    where: {
      name: director,
    },
  })
    .then((data) => {
      if (data === null) return Director.create({ name: director });
      return data;
    }).then((data) => {
      const { id } = data.dataValues;
      return id;
    })
    .then((id) => {
      movieObject.director_id = id;
      return Movie.create(movieObject);
    })
    .then((addedMovie) => {
      res.status(200).json({ msg: 'data is stored successfully', addedMovie });
    })
    .catch((error) => {
      res.status(500).json({ msg: 'server error', error });
    });
}

function updateMovie(req, res) {
  const { movieId } = req.params;
  const updatedMovie = req.body;
  const updatedDirector = updatedMovie.Director;
  const movieObject = {
    rank: updatedMovie.Rank,
    title: updatedMovie.Title,
    description: updatedMovie.Description,
    runtime: updatedMovie.Runtime,
    genre: updatedMovie.Genre,
    rating: updatedMovie.Rating,
    metascore: updatedMovie.Metascore === 'NA' ? 0 : updatedMovie.Metascore,
    votes: updatedMovie.Votes,
    gross_earning: updatedMovie.Gross_Earning_in_Mil === 'NA' ? 0 : updatedMovie.Gross_Earning_in_Mil,
    actor: updatedMovie.Actor,
    year: updatedMovie.Year,
  };
  Movie.findOne({
    where: {
      id: movieId,
    },
  }).then((data) => {
    if (data !== null) {
      return Director.findOne({
        where: {
          name: updatedDirector,
        },
      })
        .then((findDirector) => {
          if (findDirector === null) return Director.create({ name: updatedDirector });
          return findDirector;
        }).then((directorDetail) => {
          const { id } = directorDetail.dataValues;
          return id;
        }).then((directorId) => {
          movieObject.director_id = directorId;
          return Movie.update(movieObject, {
            where: {
              id: movieId,
            },
          });
        })
        .then((updatedResult) => {
          res.status(200).json({ msg: 'data is updated successfully', updatedResult });
        });
    }
    return res.status(404).json({ msg: `id is not found ${movieId}` });
  }).catch((error) => {
    res.status(500).json({ msg: 'server error', error });
  });
}

function deleteMovieById(req, res) {
  const { movieId } = req.params;
  Movie.findOne({
    where: {
      id: movieId,
    },
  }).then((data) => {
    if (data !== null) {
      return Movie.destroy({
        where: {
          id: movieId,
        },
      });
    }
    return data;
  }).then((deletedMovie) => {
    if (deletedMovie === null) {
      res.status(404).json({ msg: `id is not found ${movieId}` });
    } else {
      res.status(200).json({ msg: 'data is deleted successfully', deletedMovie });
    }
  })
    .catch((error) => {
      res.status(500).json({ msg: 'server error', error });
    });
}

module.exports = {
  getAllMovies,
  getMovieById,
  addNewMovie,
  updateMovie,
  deleteMovieById,
};
