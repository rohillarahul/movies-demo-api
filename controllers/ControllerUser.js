const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { isValidRegistration, isValidLogin } = require('../utils/validation schema/ValidationUser');
const User = require('../models/User');

// registering new user
const registration = (req, res) => {
  // validate new user information
  const { error } = isValidRegistration(req.body);
  if (error) {
    return res.status(403).send(error.details[0].message);
  }
  const user = {
    name: req.body.name,
    email: req.body.email,
    password: req.body.password,
    role: req.body.role,
  };

  User.sync(() => User.sync({ force: true }))
    .then(() => {
      const emailExist = User.findOne({
        where: {
          email: user.email,
        },
      });
      return emailExist;
    })
    .then((isExist) => {
      if (isExist === null) {
        return bcrypt.genSalt(10);
      }
      return new Promise((resolve, reject) => {
        reject(new Error('email is already exist'));
      });
    }).then((salt) => {
      const hashPassword = bcrypt.hash(user.password, salt);
      return hashPassword;
    })
    .then((hashPassword) => {
      user.password = hashPassword;
      return User.create(user);
    })
    .then(() => {
      res.json({
        email: user.email, id: user.id, role: user.role, register: true,
      });
    })
    .catch((error) => res.send(error.message));
};

// login for user
const login = async (req, res) => {
  const { error } = isValidLogin(req.body);
  if (error) {
    res.status(403).send(error.details[0].message);
  }
  const loginUser = req.body;
  const userAvailable = await User.findOne({
    where: {
      email: loginUser.email,
    },
  });

  if (userAvailable === null) {
    return res.json({ msg: 'email is not exists', login: false });
  }
  const validPass = await bcrypt.compare(req.body.password, userAvailable.password);

  if (!validPass) {
    return res.json({ msg: 'not a legal user', login: false });
  }
  const token = jwt.sign({
    id: userAvailable.id,
    role: userAvailable.role,
  }, process.env.SECRET_KEY);

  return res.header('auth-token', token).json({ msg: 'legal user', login: true });
};

module.exports.registration = registration;
module.exports.login = login;
