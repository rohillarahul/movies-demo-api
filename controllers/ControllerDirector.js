const Director = require('../models/Director');

function getAllDirectors(req, res) {
  Director.findAll().then((allDirector) => {
    res.status(400).json(allDirector);
  }).catch((error) => {
    res.status(500).json(error);
  });
}

function getDirectorById(req, res) {
  const { id } = req.params;
  try {
    (async () => {
      const director = await Director.findOne({
        where: {
          id,
        },
      });
      if (director !== null) {
        res.status(200).json(director);
      } else res.status(400).json({ msg: 'id is not found' });
    })();
  } catch (error) {
    res.status(500).json({ msg: 'server error' });
  }
}

function addNewDirector(req, res) {
  const director = {
    ...req.body,
  };

  try {
    if (!director.name) {
      res.status(400).json({ msg: 'null name is not allowed' });
    } else {
      (async () => {
        const addedDirector = await Director.create(director);
        res.status(200).json({ msg: 'data is stored successfully', addedDirector });
      })();
    }
  } catch (error) {
    res.status(500).json({ msg: 'server error' });
  }
}

function updateDirector(req, res) {
  const { directorId } = req.params;
  const updatedDirector = req.body;
  updatedDirector.id = directorId;
  if (!updatedDirector.name) {
    res.status(400).json({ msg: 'null name is not allowed' });
  }

  Director.findOne({
    where: {
      id: directorId,
    },
  }).then((data) => {
    if (data !== null) {
      (async () => {
        const updatedResult = await Director.update(updatedDirector, {
          where: {
            id: directorId,
          },
        });
        return res.status(200).json({ msg: 'data is updated successfully', updatedResult });
      })();
    } else {
      return res.status(404).json({ msg: `id is not found ${directorId}` });
    }
  }).catch((error) => {
    res.status(500).json({ msg: 'server error', error });
  });
}

function deleteDirectorById(req, res) {
  const { directorId } = req.params;
  Director.findOne({
    where: {
      id: directorId,
    },
  }).then((data) => {
    if (data !== null) {
      return Director.destroy({
        where: {
          id: directorId,
        },
      });
    }
    return data;
  }).then((data) => {
    if (data === null) {
      res.status(404).json({ msg: `id is not found ${directorId}` });
    } else {
      res.status(200).json({ msg: 'data is deleted successfully', data });
    }
  })
    .catch((error) => {
      res.status(500).json({ msg: 'server error', error });
    });
}

module.exports = {
  getAllDirectors,
  getDirectorById,
  addNewDirector,
  updateDirector,
  deleteDirectorById,
};
