const Joi = require('@hapi/joi');

function isValidRegistration(user) {
  const schema = Joi.object({
    name: Joi.string().min(3).max(255).required(),
    email: Joi.string().email().required(),
    password: Joi.string().min(6).required(),
    role: Joi.string().required(),
  });
  return schema.validate(user);
}

function isValidLogin(loginUser) {
  const loginSchema = Joi.object({
    email: Joi.string().email().required(),
    password: Joi.string().min(6).required(),
  });

  return loginSchema.validate(loginUser);
}

module.exports.isValidRegistration = isValidRegistration;
module.exports.isValidLogin = isValidLogin;
