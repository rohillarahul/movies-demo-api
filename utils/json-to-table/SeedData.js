const Director = require('../../models/Director');
const Movie = require('../../models/Movie');
const moviesData = require('../../data/movies.json');

function createTable() {
  Director.hasMany(Movie, {
    foreignKey: 'director_id',
  });
  Movie.belongsTo(Director, {
    foreignKey: 'director_id',
  });

  return Director.sync({ force: true }).then(() => Movie.sync({ force: true }));
}

function insertIntoDirector(director) {
  return Director.create(director);
}

function getDirectorIdByName(name) {
  return Director.findOne({
    where: {
      name,
    },
  });
}

function insertIntoMovie(movie) {
  const movieObject = {
    rank: movie.Rank,
    title: movie.Title,
    description: movie.Description,
    runtime: movie.Runtime,
    genre: movie.Genre,
    rating: movie.Rating,
    metascore: movie.Metascore === 'NA' ? 0 : movie.Metascore,
    votes: movie.Votes,
    gross_earning: movie.Gross_Earning_in_Mil === 'NA' ? 0 : movie.Gross_Earning_in_Mil,
    actor: movie.Actor,
    year: movie.Year,
  };
  const director = movie.Director;
  return getDirectorIdByName(director)
    .then((data) => {
      if (data === null) return insertIntoDirector({ name: director });
      return data;
    }).then((data) => {
      const { id } = data.dataValues;
      return id;
    })
    .then((id) => {
      movieObject.director_id = id;
      return Movie.create(movieObject);
    });
}

let i = 0;
createTable()
  .then(() => {
    setInterval(() => {
      insertIntoMovie(moviesData[i]).then(() => {
        i++;
        if (i === moviesData.length) {
          clearInterval();
        }
      });
    }, (1000));
  });
