# REST API example(movies-api)

This is a simple movies-api for demo purpose, it is developed using nodejs, express, mysql technologies, it is hosted on heroku platform,
for use this api you don't need any set-up, you only need Postman tool.

## Description
Using this api you can perform, CRUD for movies and director of the movies entity. See the below doc for using it.


# REST API For Director
1.Get all directors
`GET api /directors`
https://mysql-movies-api.herokuapp.com/api/directors

2.Get director by id
`GET api /directors/id`
https://mysql-movies-api.herokuapp.com/api/directors/id

3.Add a new director
`POST api /directors/id`
step-1. set request to post request
step-2. set content-type to json in header, 
step-3.  set body data to raw and then send the below object into request body 

        {
            "name":"director_name"
        }

        
        
4.delete director by id
`DELETE api /directors/id`


# REST API For Movie
1.Get all Movies
`GET api /movie`

2.Get movie by id
`GET api /movie/id`
https://mysql-movies-api.herokuapp.com/api/directors/id

3.Add a new movie
`POST api /movie/`
step-1. set request to post request
step-2. set content-type to json in header, 
step-3.  set body data to raw and then send the below object into request body, you can change the value of below object's 
NOTE: If the director is exists then no duplicate director will show in /api/directors api.

        {
        "Rank": 50,
        "Title": "Casablanca",
        "Description": "A cynical nightclub owner protects an old flame and her husband from Nazis in Morocco.A cynical nightclub owner protects an old flame and her husband from Nazis in Morocco.",
        "Runtime": 102,
        "Genre": "Drama",
        "Rating": 8.5,
        "Metascore": 100,
        "Votes": "441864",
        "Gross_Earning_in_Mil": "1.02",
        "Actor": "Humphrey Bogart",
        "Year": "1942",
        "Director": "Michael Curtiz"
    }

        
        
4.delete movie by id
`DELETE api /movie/id`


