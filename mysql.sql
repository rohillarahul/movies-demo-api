ALTER TABLE users MODIFY id int NOT NULL AUTO_INCREMENT PRIMARY KEY;

create table movie(id int primary key, rank int default null, title varchar(100) not null, description text not null, runtime int not null, genre varchar(50) not null, rating  float not null,metascore int, votes decimal(7) not null,gross_earning decimal(5,2) not null,director_id int,actor_id int not null,year decimal(4));


create table actor(id int primary key auto_increment, name varchar(100) not null)

create table director(id int primary key auto_increment, name varchar(100) not null)

create table acting_by(movie_id int not null,actor_id int not null,foreign key(movie_id) references movie(id), foreign key(actor_id) references actor(id));