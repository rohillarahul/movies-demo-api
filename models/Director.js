const {
  Sequelize, Model,
} = require('sequelize');
const env = require('../config/env');

// console.log(connection);
const config = {
  host: env.host,
  dialect: env.dialect,
  define: {
    freezeTableName: true,
  },
};
const sequelize = new Sequelize(env.database,
  env.username,
  env.password, config);

class Director extends Model {}

Director.init({
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    allowNull: false,
    autoIncrement: true,
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
}, {
  sequelize,
  modelName: 'director',
  timestamps: false,
});

module.exports = Director;
