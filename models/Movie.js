const {
  Sequelize, DataTypes, Model,
} = require('sequelize');
const env = require('../config/env');

const Director = require('./Director');

const config = {
  host: env.host,
  dialect: env.dialect,
  define: {
    freezeTableName: true,
  },
};
const sequelize = new Sequelize(env.database, env.username, env.password, config);

class Movie extends Model {}

Movie.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false,
  },
  rank: {
    type: DataTypes.INTEGER,
    defaultValue: 0,
  },
  title: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  description: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  runtime: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  genre: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  rating: {
    type: DataTypes.FLOAT,
    allowNull: false,
  },
  metascore: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  votes: {
    type: DataTypes.DECIMAL(10, 0),
    allowNull: false,
  },
  gross_earning: {
    type: DataTypes.DECIMAL(5, 2),
    allowNull: false,
  },
  actor: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  director_id: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  year: {
    type: DataTypes.DECIMAL(4, 0),
    allowNull: true,
  },
}, {
  sequelize,
  modelName: 'movie',
  timestamps: false,
});

Director.hasMany(Movie, {
  foreignKey: 'director_id',
});
Movie.belongsTo(Director, {
  foreignKey: 'director_id',
});

module.exports = Movie;
