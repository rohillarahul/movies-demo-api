const {
  Sequelize, Model, DataTypes,
} = require('sequelize');
const env = require('../config/env');

const config = {
  host: env.host,
  dialect: env.dialect,
  define: {
    freezeTableName: true,
  },
};
const sequelize = new Sequelize(env.database,
  env.username,
  env.password, config);

class User extends Model {}

User.init({
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey: true,
    autoIncrement: true,
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  email: {
    type: DataTypes.STRING,
    required: true,
    max: 255,
    min: 6,
  },
  password: {
    type: DataTypes.STRING,
    required: true,
    max: 1024,
    min: 6,
  },
  role: {
    type: DataTypes.STRING,
    allowNull: false,
    defaultValue: 'general',
  },
}, {
  sequelize,
  modelName: 'user',
  timestamps: false,
});

module.exports = User;
