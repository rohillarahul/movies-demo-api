const dotenv = require('dotenv');
const env = require('./env.js');

console.log(JSON.stringify(env));
try {
  const result = dotenv.config();
  if (result.error) {
    throw result.error;
  }
  const { parsed: envs } = result;
  module.exports = envs;
} catch (error) {
  console.log(error.message);
}
