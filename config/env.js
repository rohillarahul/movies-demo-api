// for hosting on locally you have to put all database details of your personal credentials, I have pushed the different branch on heroku, which is not persent here.
const env = {
  database: 'database_name',
  username: 'user_name',
  password: 'password',
  host: 'host_name',
  port: 3306,
  dialect: 'mysql',
};
module.exports = env;
