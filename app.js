const express = require('express');
const bodyParser = require('body-parser');
// require('./utils/json-to-table/SeedData');

const directorRoutes = require('./routes/RoutesDirector');
const movieRoutes = require('./routes/RoutesMovie');
const userRoutes = require('./routes/RoutesUser');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/api/directors', directorRoutes);

app.use('/api/movie', movieRoutes);

app.use('/api/user', userRoutes);

app.use('/', (req, res) => {
  res.json('Hello world');
});

const PORT = process.env.PORT || 3000;

app.listen(PORT, () => {
  console.log(`server started on PORT ${PORT}`);
});
