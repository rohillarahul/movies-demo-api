const express = require('express');
const movieAction = require('../controllers/ControllerMovie');
const validateMovieMiddleware = require('../middlewares/ValidationMovie');

const router = express.Router();

const {
  getAllMovies, getMovieById, addNewMovie, updateMovie, deleteMovieById,
} = movieAction;

router.use(validateMovieMiddleware);

// get all movies api routes
router.get('/', getAllMovies);

// get movies by id route
router.get('/:movieId', getMovieById);

// add movie api
router.post('/', addNewMovie);

// update movie api
router.put('/:movieId', updateMovie);

// delete movie by id
router.delete('/:movieId', deleteMovieById);

module.exports = router;
