const express = require('express');
const directorAction = require('../controllers/ControllerDirector');
const validateDirectorMiddleware = require('../middlewares/ValidationDirector');

const directorRouter = express.Router();

const {
  getAllDirectors, getDirectorById, addNewDirector, updateDirector, deleteDirectorById,
} = directorAction;

directorRouter.use(validateDirectorMiddleware);

// get all director api routes
directorRouter.get('/', getAllDirectors);

// get director by id route
directorRouter.get('/:id', getDirectorById);

// add new director api
directorRouter.post('/', addNewDirector);

// update director api
directorRouter.put('/:directorId', updateDirector);

// delete director by id
directorRouter.delete('/:directorId', deleteDirectorById);

module.exports = directorRouter;
