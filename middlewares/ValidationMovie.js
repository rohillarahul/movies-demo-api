const Joi = require('@hapi/joi');

const validateMovie = (user) => {
  const schema = Joi.object({
    Rank: Joi.number(),
    Title: Joi.string().min(6).max(100).required(),
    Description: Joi.string().min(100),
    Runtime: Joi.number().integer(),
    Genre: Joi.string().min(3).max(100),
    Rating: Joi.number().min(0).max(10),
    Metascore: Joi.number().max(100),
    Votes: Joi.number().positive(),
    Gross_Earning_in_Mil: Joi.number().precision(2),
    Director: Joi.string().min(6).max(100).required(),
    Actor: Joi.string().min(6).max(100).required(),
    Year: Joi.number().less(10000).greater(999),
  });
  return schema.validate(user);
};

function validateMovieMiddleware(req, res, next) {
  if (req.method === 'POST' || req.method === 'PUT') {
    const { error } = validateMovie(req.body);
    if (error !== null) {
      res.json({ error: error.details[0].message });
    } else {
      next();
    }
  } else {
    next();
  }
}

module.exports = validateMovieMiddleware;
