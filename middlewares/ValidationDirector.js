const Joi = require('@hapi/joi');

const validateDirector = (user) => {
  const schema = Joi.object({
    name: Joi.string().min(6).max(100).required(),
  });
  return schema.validate(user);
};

function validateDirectorMiddleware(req, res, next) {
  if (req.method === 'POST' || req.method === 'PUT') {
    const { error } = validateDirector(req.body);
    if (error !== null) {
      res.json({ error: error.details[0].message });
    } else {
      next();
    }
  } else {
    next();
  }
}

module.exports = validateDirectorMiddleware;
